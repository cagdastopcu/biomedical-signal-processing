clear all;
close all;
clc,

%% HRV

load('RR.mat')

h = plot(RR,'ro','linestyle','none','markers',8)
set(h, 'MarkerFaceColor', get(h, 'Color'));

%% Standard deviation of RR

SDNN = std(RR); %msec

%% RMSSD Root Mean Square of 
% the Successive Differences
% msec
RMSSD = sqrt(mean(diff(RR).*diff(RR)))

%% NN50

alpha = 50; %ms

NN50 = sum(abs(diff(RR)) >= alpha)

%% PNNAlpha PNN50
alpha = 50; %ms

pNN50 = sum(abs(diff(RR)) >= alpha)/length(diff(RR))
