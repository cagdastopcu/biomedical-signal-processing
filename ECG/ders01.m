%% Ders 1
%% Isaret olusturma
%% 3 adet sinus ve Welch metoduyla PSD

t = -4*pi:0.01:4*pi;

x1 = 100*sin(2*pi*t);

x3 = 50*sin(2*pi*3*t);

x5 = 35*sin(2*pi*5*t);

gurultu = 200*randn(1,length(x1));

toplamIsaret = x1 + x3 + x5 + gurultu;

window = round(length(toplamIsaret)/4);

noverlap = window/2;

freq = 0:0.01:7;

fs = 100; %Hz

[pxx,f] = pwelch(toplamIsaret,window,...
    noverlap,freq,fs);

plot(f,10*log10(pxx),'linewidth', 3)

