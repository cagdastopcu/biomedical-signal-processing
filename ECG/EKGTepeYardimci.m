clear all
load('ECGraw.mat')

index = 1:300000;

[pks,locs] = findpeaks(ecg(index),...
    'minpeakdistance',100,...
    'MINPEAKHEIGHT',4000);

impulseECG = zeros(1,length(ecg(index)));
impulseECG(locs) = ecg(locs);
figure
plot(ecg(index));
hold on
stem(impulseECG,'r','linewidth',2)

RR = diff(locs);
figure
plot(RR,'ro')

HR = (60000./RR)

figure,
plot(HR,'o')

meanHR = mean(HR)
