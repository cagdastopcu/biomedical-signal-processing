%% Ders 1
%% Isaret olusturma

t = -4*pi:0.01:4*pi;

x1 = 100*sin(2*pi*t);

x3 = 50.5*sin(2*pi*3*t);

x5 = 50*sin(2*pi*5*t);

gurultu = 200*randn(1,length(x1));

toplam = x1+x3+x5+gurultu;

plot(t,toplam,'linewidth',3);

window = round(length(x1)/4);

noverlap = round(window/2);

freqs = 0:0.01:7;

fs = 100;
nsm=9; % smoothing with running Hann window of nsm points (odd!!)  
psdIsaret = pwelch(toplam,window,noverlap,freqs,fs);

plot(freqs,10*log10(psdIsaret),'r')