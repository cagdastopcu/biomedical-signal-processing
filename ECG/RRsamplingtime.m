clear all;
close all;
clc,

%% HRV

load('RR.mat')

HR = 60000./RR;

Fs = 7;

ts = (Fs^-1)*1000;

t = 0:ts:sum(RR);
    tx = cumsum(RR)-RR;
    t(t<tx(1)) = [];
    t(t>tx(end)) = [];
    HR_7Hz = interp1(tx, HR, t, 'spline');
    
    subplot(2,1,1)
    plot(HR,'o')
    subplot(2,1,2)
    plot(HR_7Hz,'ro')
    
    axis tight
    
%     window = round(length(HR_7Hz)/4);

    window = 2000;

    noverlap = round(window/2);

    freqs = 0:0.0035:3.5;

    fs = 7;
    
    [PSD,f] = pwelch(HR_7Hz-mean(HR_7Hz),window,noverlap,freqs,fs);

    plot(freqs,10*log10(PSD),'r')
    
    [VLF, LF, HF, LFHFratio, nLF, nHF] = frequencyFeatures(f,PSD);      
    